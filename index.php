<link rel="stylesheet" href="style.css">

<?php

require_once "vendor/autoload.php";
require_once "Model/Minesweeper.php";

$latte = new Latte\Engine;

try {
    $minesweeper = new Minesweeper(20, 20, 20);
    $latte->render('template/minefield.latte', ['minesweeper' => $minesweeper]);
} catch (Exception $e) {
    echo $e->getMessage();
}
