<?php

class Minesweeper
{
    public const MINE_SYMBOL = 'x';

    /**
     * @var int
     */
    private int $rows;

    /**
     * @var int
     */
    private int $columns;

    /**
     * @var int
     */
    private int $minesQuantity;

    /**
     * @var array
     */
    private array $mineField = [];

    /**
     * @param int $columns
     * @param int $rows
     * @param int $minesQuantity
     * @throws Exception
     */
    public function __construct(
        int $columns,
        int $rows,
        int $minesQuantity
    )
    {
        $this->setRows($rows);
        $this->setColumns($columns);
        $this->setMinesQuantity($minesQuantity);

        $this->createField();
        $this->populateMines();
        $this->calculateNumberClues();
    }

    /**
     * @param int $rows
     * @return void
     * @throws Exception
     */
    public function setRows(int $rows): void
    {
        if ($rows <= 0) {
            throw new Exception('You are trying to set invalid number of rows, should be > 0');
        }

        $this->rows = $rows;
    }

    /**
     * @param int $columns
     * @return void
     * @throws Exception
     */
    public function setColumns(int $columns): void
    {
        if ($columns <= 0) {
            throw new Exception('You are trying to set invalid number of columns, should be > 0');
        }

        $this->columns = $columns;
    }

    /**
     * @param int $minesQuantity
     * @return void
     * @throws Exception
     */
    public function setMinesQuantity(int $minesQuantity): void
    {
        if ($minesQuantity > $this->rows * $this->columns) {
            throw new Exception('You are trying to set invalid number of mines, should be less than (rows * columns)');
        }

        $this->minesQuantity = $minesQuantity;
    }

    /**
     * @return void
     */
    private function createField(): void
    {
        for ($column = 0; $column < $this->columns; $column++) {
            for ($row = 0; $row < $this->rows; $row++) {
                $this->mineField[$row][$column] = 0;
            }
        }
    }

    /**
     * @return void
     */
    private function populateMines(): void
    {
        $minesCounter = 0;
        while ($minesCounter < $this->getMinesQuantity()) {
            $x = rand(0, $this->getColumns() - 1);
            $y = rand(0, $this->getRows() - 1);

            if ($this->mineField[$y][$x] !== $this::MINE_SYMBOL) {
                $this->mineField[$y][$x] = $this::MINE_SYMBOL;
                $minesCounter++;
            }
        }
    }

    /**
     * @param int $x
     * @param int $y
     * @return void
     */
    private function increaseFieldValue(int $x, int $y): void
    {
        if ($x >= 0 &&
            $x < $this->getColumns() &&
            $y >= 0 &&
            $y < $this->getRows() &&
            is_numeric($this->mineField[$y][$x])
        ) {
            $this->mineField[$y][$x]++;
        }
    }

    /**
     * @param int $x
     * @param int $y
     * @return void
     */
    private function populateCluesAround(int $x, int $y): void
    {
        for ($rx = $x - 1; $rx <= $x + 1; $rx++) {
            for ($ry = $y - 1; $ry <= $y + 1; $ry++) {
                $this->increaseFieldValue($rx, $ry);
            }
        }
    }

    /**
     * @return void
     */
    private function calculateNumberClues(): void
    {
        for ($column = 0; $column < $this->columns; $column++) {
            for ($row = 0; $row < $this->rows; $row++) {
                if ($this->mineField[$row][$column] === $this::MINE_SYMBOL) {
                    $this->populateCluesAround($column, $row);
                }
            }
        }
    }

    /**
     * @return int
     */
    public function getRows(): int
    {
        return $this->rows;
    }

    /**
     * @return int
     */
    public function getColumns(): int
    {
        return $this->columns;
    }

    /**
     * @return int
     */
    public function getMinesQuantity(): int
    {
        return $this->minesQuantity;
    }

    /**
     * @return array
     */
    public function getMineField(): array
    {
        return $this->mineField;
    }

}